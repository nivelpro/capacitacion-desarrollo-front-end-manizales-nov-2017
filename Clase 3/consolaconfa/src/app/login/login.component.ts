import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(private router:Router) {

  }
  ngOnInit() {
  }
  validarUsuario(login:NgForm){
  	console.log(login.value);

  	if (login.value.usuario == "admin" && login.value.contrasena =="123") {
  		localStorage.setItem("logged", "true");
  		this.router.navigate(['/dash']);
  	}else{
  		alert("Usuario No Valido");
  		localStorage.setItem("logged", "false");
 		login.reset();
  	}
  }
}
