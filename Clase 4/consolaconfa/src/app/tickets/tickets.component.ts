import { Component, OnInit } from '@angular/core';
import { ConsolaService} from '../consola.service';


declare var emailjs:any;

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.css']
})
export class TicketsComponent implements OnInit {

	beneficiarios:Object

  constructor(public servicioConsola:ConsolaService) { }

  ngOnInit() {
  	this.servicioConsola.cargarWS().subscribe(
  		datosServicio=>{
  			this.beneficiarios = datosServicio;
  			console.log(this.beneficiarios);
  		}
  	)
  }
  enviarCorreo(ticket){
    
  	var datos = {
  		beneficiario: ticket.value.beneficiario,
  		asunto: ticket.value.asunto,
  		message_html: ticket.value.mensaje
  	};
  	emailjs.init("user_8hbE9sktIf8kipARLtAot");
  	emailjs.send('gmail', "template_5xDF1VhS", datos).then(
  		function(respuesta){
  			alert("Su ticket fue enviado, espera pronta respuesta");
  		},
  		function(error){
  			console.log(error);
  			alert("Error en el envio del ticket");
  		}
  	);
  }

}
