import { Injectable } from '@angular/core';
import "rxjs/Rx";
import { Observable } from "rxjs/Observable";

import { Http, Response } from '@angular/http';

@Injectable()
export class ConsolaService {

	jsonWebService:Object;

	constructor(private http:Http) {
		this.jsonWebService = {};
	}

	cargarWS():Observable<Object>{
		
		return this.http.request("http://app.confa.co:8081/auxilioCalzadoWS/rest/calzado/consultaCalzado/513639/bata1").map(
			(respuestaWS:Response)=>{
				this.jsonWebService = respuestaWS.json();
				return this.jsonWebService;
			}
		)
	}

}
