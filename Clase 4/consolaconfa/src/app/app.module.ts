import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes} from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './header/header.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { ContentComponent } from './content/content.component';
import { MapasComponent } from './mapas/mapas.component';
import { TicketsComponent } from './tickets/tickets.component';


import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { environment } from '../environments/environment';


import { AgmCoreModule } from '@agm/core';

import { ConsolaService } from './consola.service';


const rutas:Routes = [
	{ path: 'login', component: LoginComponent },
	{ path: 'dash', component: DashboardComponent, children:[
    {path: 'estadisticas', component: ContentComponent},
    {path: 'mapas', component: MapasComponent},
    {path: 'tickets', component: TicketsComponent},
    {path: '', redirectTo:'estadisticas', pathMatch:'full'}
  ]},
	{ path: '', redirectTo:'login', pathMatch:'full'},
	{ path: '**', redirectTo:'login'}
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    HeaderComponent,
    SidemenuComponent,
    ContentComponent,
    MapasComponent,
    TicketsComponent
  ],
  imports: [
  	NgbModule.forRoot(),
  	RouterModule.forRoot(rutas),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    ChartsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDfmKJFufwr-bjdbC3rpr4iIUbe0up0ZmI'
    })
  ],
  providers: [ConsolaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
