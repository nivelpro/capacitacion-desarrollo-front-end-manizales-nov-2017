import { TestBed, inject } from '@angular/core/testing';

import { ConsolaService } from './consola.service';

describe('ConsolaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConsolaService]
    });
  });

  it('should be created', inject([ConsolaService], (service: ConsolaService) => {
    expect(service).toBeTruthy();
  }));
});
