import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

	graficaData:Array<any>;
	graficaLabels:Array<any>;
	graficaOptions:any;
	graficaColors:Array<any>;
	graficaLegend:boolean;
	graficaChartType:string;


	infoData:number[];
	infoLabels:Array<any>;
	infoOptions:any;
	infoColors:Array<any>;
	infoLegend:boolean;
	infoChartType:string;

  	datos: Observable<any[]>;

  	constructor(db:AngularFireDatabase) { 
  		this.datos = db.list('datos').valueChanges();
  	}

  	ngOnInit() {
  		this.graficaData = [
  			{
  				data:[100, 500, 10, 40],
  				label:"2017"
  			},
  			{
  				data:[45, 134, 106, 40],
  				label:"2016"
  			},
  			{
  				data:[50, 65, 78, 89],
  				label:"2015"
  			}
  		];
  		this.graficaLabels=["Ene", "Feb", "Mar", "Abr"];
  		this.graficaOptions = {
  			showLines:true,
  			animation:{
  				duration:2000
  			}

  		};
  		this.graficaColors = [
  			{
  				backgroundColor: "rgba(134, 204, 216, 0.5)",
  				borderColor: "rgb(74, 117, 124)",
  				pointBackgroundColor: "#3a97a5",
  				pointBorderColor:"#064c56"

  			},
  			{
  				backgroundColor: "rgba(134, 204, 216, 0.5)",
  				borderColor: "rgb(74, 117, 124)",
  				pointBackgroundColor: "#3a97a5",
  				pointBorderColor:"#064c56"

  			}
  		];
  		this.graficaLegend = true;
  		this.graficaChartType = "line";
  		//this.graficaChartType = "bar";


  		this.infoData=[100, 60, 79];
  		this.infoLabels=["Facebook", "Directo", "Correo"];
  		this.infoChartType = "pie"

  	}

}
