// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyByftItD7qxGASHT1EgCg0Km-ADaJAM-GM',
    authDomain: 'confa-bd.firebaseapp.com',
    databaseURL: 'https://confa-bd.firebaseio.com',
    projectId: 'confa-bd',
    storageBucket: 'confa-bd.appspot.com',
    messagingSenderId: '514224610474'
  }
};