import { Component, OnInit } from '@angular/core';
import { BibliotecalibrosService } from "../bibliotecalibros.service"

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  constructor(public elservicio:BibliotecalibrosService) { }

  ngOnInit() {
  }

}
