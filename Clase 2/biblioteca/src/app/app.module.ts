import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { EncabezadoComponent } from './encabezado/encabezado.component';
import { AcercaDeComponent } from './acerca-de/acerca-de.component';
import { DetallesComponent } from './detalles/detalles.component';
import { Error404Component } from './error404/error404.component';
import { InicioComponent } from './inicio/inicio.component';
import { ListaDeLibrosComponent } from './lista-de-libros/lista-de-libros.component';

import { BibliotecalibrosService } from './bibliotecalibros.service';

const rutasApp:Routes=[
  { path: 'lista-libros', component: ListaDeLibrosComponent},
  { path: 'acerca-de', component: AcercaDeComponent},
  { path: 'detalles/:libroId', component: DetallesComponent},
  { path: 'detalles', redirectTo: 'lista-libros'},
  { path: '', component: InicioComponent, pathMatch: 'full'},
  { path: '404', component: Error404Component},
  { path: '**', redirectTo:'404'}
]

@NgModule({
  declarations: [
    AppComponent,
    EncabezadoComponent,
    AcercaDeComponent,
    DetallesComponent,
    Error404Component,
    InicioComponent,
    ListaDeLibrosComponent
  ],
  imports: [
    RouterModule.forRoot(rutasApp),
    FormsModule,
    HttpModule,
    BrowserModule
  ],
  providers: [BibliotecalibrosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
