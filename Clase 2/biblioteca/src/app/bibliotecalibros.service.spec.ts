import { TestBed, inject } from '@angular/core/testing';

import { BibliotecalibrosService } from './bibliotecalibros.service';

describe('BibliotecalibrosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BibliotecalibrosService]
    });
  });

  it('should be created', inject([BibliotecalibrosService], (service: BibliotecalibrosService) => {
    expect(service).toBeTruthy();
  }));
});
