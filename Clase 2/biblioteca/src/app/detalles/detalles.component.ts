import { Component, OnInit } from '@angular/core';
import { BibliotecalibrosService } from '../bibliotecalibros.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.component.html',
  styleUrls: ['./detalles.component.css']
})
export class DetallesComponent implements OnInit {

	libros:Array<Object>;
	libroDetalle: Object;
	libroId:Number;

  	constructor(private miservicio:BibliotecalibrosService, private ruta:ActivatedRoute) {
  		this.libros = [];
  		this.libroDetalle = {};
  		this.miservicio.cargarLibrosJSON().subscribe(
  			datosRespuesta=>{
  				this.libros = datosRespuesta;
  				this.ejecutarBusqueda();
  			}
  		);
  	}

  	filtroPorId(libro){
  		return libro.id==this;
  	}

  	ejecutarBusqueda(){
  		this.ruta.params.subscribe(
  			parametro=>{
  				this.libroId=parametro["libroId"];
  				this.libroDetalle = this.libros.filter(this.filtroPorId, this.libroId)[0];
  			}

  		)
  	}

    ngOnInit() {
	  }

    agregarFavorito(libroFavorito){
      this.miservicio.agregarLibrosFavoritos(libroFavorito);
      alert("Su libro fue adicionado exitosamente!");
    }

}
