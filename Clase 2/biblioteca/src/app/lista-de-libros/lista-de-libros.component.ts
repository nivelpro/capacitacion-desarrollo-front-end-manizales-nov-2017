import { Component, OnInit } from '@angular/core';

import { BibliotecalibrosService } from '../bibliotecalibros.service';


@Component({
  selector: 'app-lista-de-libros',
  templateUrl: './lista-de-libros.component.html',
  styleUrls: ['./lista-de-libros.component.css']
})
export class ListaDeLibrosComponent implements OnInit {

	libros:Array<Object>;
	cargando:Boolean;
	errorHttp:Boolean;

  	constructor(private miservicio:BibliotecalibrosService) { }

  	ngOnInit() {
  		this.cargando=true;
  		this.miservicio.cargarLibrosJSON().subscribe(
  			datosServicio => {
  				this.libros = datosServicio;
  				this.cargando = false;
  			}
  		)
  	}

}
