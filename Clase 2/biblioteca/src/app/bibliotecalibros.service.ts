import { Injectable } from '@angular/core';
import "rxjs/Rx";
import { Observable } from 'rxjs/Observable';

import { Http, Response } from '@angular/http';


@Injectable()
export class BibliotecalibrosService {

	libros:Array<Object>;
	librosFavoritos:Array<Object>;
	errorHttp:Boolean;
	cargando:Boolean;

	constructor(private http:Http) {
		this.libros = [];
		this.librosFavoritos = [];
	}

	cargarLibrosJSON():Observable<Array<Object>>{
		return this.http.request('/assets/json/lista-de-libros.json')
		.map(
			(respuesta:Response)=>{
				return respuesta.json()
			}
		);
	}

	agregarLibrosFavoritos(nuevoLibro){
		this.librosFavoritos.push(nuevoLibro);
	}

	mostrarLibrosFavoritos(){
		if (this.librosFavoritos.length>0) {
			return this.librosFavoritos;
		}else{
			return false;
		}
	}

}





