import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BannerComponent } from './banner/banner.component';
import { ListadoCabanasComponent } from './listado-cabanas/listado-cabanas.component';
import { ReservaComponent } from './reserva/reserva.component';
import { InicioComponent } from './inicio/inicio.component';

import { CabanasService } from './cabanas.service';

const rutasApp:Routes=[
  { path: '', component: InicioComponent, pathMatch: 'full'},
  { path: '**', redirectTo:''}
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BannerComponent,
    ListadoCabanasComponent,
    ReservaComponent,
    InicioComponent
  ],
  imports: [
    RouterModule.forRoot(rutasApp),
    FormsModule,
    HttpModule,
    BrowserModule
  ],
  providers: [CabanasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
