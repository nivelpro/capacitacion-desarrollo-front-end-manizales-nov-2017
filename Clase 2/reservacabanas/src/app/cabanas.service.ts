import { Injectable } from '@angular/core';
import "rxjs/Rx";
import { Observable } from 'rxjs/Observable';

import { Http, Response } from '@angular/http';

@Injectable()
export class CabanasService {

	cabanas:Array<Object>;
	cabanasReserva:Array<Object>;
	sumaReserva:Number;
	errorHttp:Boolean;
	cargando:Boolean;

  	constructor(private http:Http) {
		this.cabanas = [];
		this.cabanasReserva = [];
		this.sumaReserva = 0;
	}

	cargarCabanasJSON():Observable<Array<Object>>{
		return this.http.request('/assets/json/datacabanas.json')
		.map(
			(respuesta:Response)=>{
				return respuesta.json()
			}
		);
	}

	agregarCabanaServicio(nuevaReserva){
		this.sumaReserva = 0;
		this.cabanasReserva.push(nuevaReserva);
		for (var i = this.cabanasReserva.length - 1; i >= 0; i--) {
			this.sumaReserva += this.cabanasReserva[i]["precio"];
		}
	}

	leerCabanaServicio(){
		if (this.cabanasReserva.length>0) {
			return this.cabanasReserva;
		}else{
			return [];
		}
	}

}
