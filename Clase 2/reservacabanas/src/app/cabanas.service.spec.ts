import { TestBed, inject } from '@angular/core/testing';

import { CabanasService } from './cabanas.service';

describe('CabanasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CabanasService]
    });
  });

  it('should be created', inject([CabanasService], (service: CabanasService) => {
    expect(service).toBeTruthy();
  }));
});
