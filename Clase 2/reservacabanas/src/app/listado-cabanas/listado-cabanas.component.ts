import { Component, OnInit } from '@angular/core';

import { CabanasService } from '../cabanas.service';
@Component({
  selector: 'app-listado-cabanas',
  templateUrl: './listado-cabanas.component.html',
  styleUrls: ['./listado-cabanas.component.css']
})
export class ListadoCabanasComponent implements OnInit {

	cabanas:Array<Object>;
	cargando:Boolean;
	errorHttp:Boolean;

	constructor(private miservicio:CabanasService) { }

	ngOnInit() {
  		this.cargando=true;
  		this.miservicio.cargarCabanasJSON().subscribe(
  			datosServicio => {
  				this.cabanas = datosServicio;
  				this.cargando = false;
  			}
  		)
  }

  adicionarCabana(nuevaCabana){
    this.miservicio.agregarCabanaServicio(nuevaCabana);
    alert("Cabaña Adicionada a su reserva");

  }

}
